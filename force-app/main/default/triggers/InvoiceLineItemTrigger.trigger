/**
 * @File Name          : InvoiceLineItemTrigger.trigger
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 28/2/2020, 3:36:18 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/2/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
trigger InvoiceLineItemTrigger on Invoice_Line_Item__c (before insert,before update,before delete,
after insert,after update,after delete,after undelete) {

    InvoiceLineItemHandler handler = new InvoiceLineItemHandler(Trigger.isExecuting, Trigger.size);

    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            
        }
        else if(Trigger.isUpdate) {
        }
        else if(Trigger.isDelete) {
        }
    }
    else if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            handler.isAfterInsert();
        }
        else if(Trigger.isUpdate) {
            handler.isAfterUpdate();
        }
        else if(Trigger.isDelete) {
            handler.isAfterDelete();
        }
        else if(Trigger.isUnDelete) {
            handler.isAfterUnDelete();
        }
    }



}