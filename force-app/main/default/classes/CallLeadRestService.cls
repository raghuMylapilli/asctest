/**
 * @File Name          : CallLeadRestService.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 31/3/2020, 12:25:27 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/3/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
public class CallLeadRestService {
    	//Wrapper Class for token parsing
    	public class deserializeResponse {
            public String id;
            public String access_token;
		}
    	//To Get Desired Result while Deserializing Output
    	public class deserializeWrapper {
            public String isSuccess;
            public String lead;
            public String status;
    	}
    	//This Method is Used to Get the Access Token
    	public static String serviceAuthenticateToken(){
            
        //Set RequestBody with all neccessary parameters for Authentication    
        String reqbody ='grant_type=password&client_id=3MVG9G9pzCUSkzZscGvKw9or6ZNIiQBHN2YAfeUYJwfMT.WIVPoMhexS7pIjO.9dC3A3K_ClWJMJA2Fic8opv';
        reqbody +='&client_secret=618C85B89C15B7B8CDD637F48AC44EEB274B7C9BF3485122E9250E9D46356FDD';
        reqbody +='&username=raghu.mylapilli@resourceful-wolf-j1flpr.com';
        reqbody +='&password=5738143Pr#9bnuUCnLeuwR5a9a1CZO8vmxe';
            
        //make a request call for Authentication
        Http httpObj = new Http();
        HttpRequest httpReqObj = new HttpRequest();
        httpReqObj.setBody(reqbody);
        httpReqObj.setEndpoint('callout:CallRestServiceToken');
        httpReqObj.setMethod('POST');
            
        //Response from Server for Accessing Token
		HttpResponse httpResObj = httpObj.send(httpReqObj);
        deserializeResponse wrappObj = (deserializeResponse)JSON.deserialize(httpResObj.getBody(),deserializeResponse.class);
        return wrappObj.access_token;               
    }
    //This Method is used to Create Lead through Rest Service   
    public static HttpResponse createLead(String FirstName, String LastName, String Email, String Phone)
    {
        String jsonstr='{"FirstName":"'+ FirstName +'","LastName":"'+ LastName 
            +'","Email":"'+ Email +'","Phone":"'+ Phone +'"}';
        //Request for the LeadService
        Http httpObj = new Http();
        HttpRequest newreq = new HttpRequest();
        String accessToken = CallLeadRestService.serviceAuthenticateToken();
        newreq.setHeader('Authorization','Bearer '+ accessToken);
        newreq.setHeader('Content-Type','application/json');
        newreq.setBody(jsonstr);
        newreq.setEndpoint('callout:CallRestService');
        newreq.setMethod('POST');
      	//GetResponse from the LeadService
        HttpResponse res2 = httpObj.send(newreq);
 		while (res2.getStatusCode() == 302) 
        {
        newreq.setEndpoint(res2.getHeader('Location'));
        res2 = new Http().send(newreq);
        }
        deserializeWrapper wrappObj = (deserializeWrapper)system.JSON.deserialize(res2.getBody(), deserializeWrapper.class);
        System.debug('createLead.isSuccess >> ' + wrappObj.isSuccess);
        System.debug('createLead.id >> ' + wrappObj.lead);
        System.debug('createLead.status >> ' + wrappObj.status);
        return res2;
    }
    //This Method is used to Update Lead through Rest Service   
    public static HttpResponse deleteLead(String FirstName, String LastName, String Email, String Phone)
    {
        String jsonstr='FirstName='+FirstName+'&LastName='+LastName+'&Email='+Email+'&Phone='+Phone;
        //Request for the Lead Service
        Http httpObj = new Http();
        HttpRequest newreq = new HttpRequest();
       	String accessToken = CallLeadRestService.serviceAuthenticateToken();
        newreq.setHeader('Authorization','Bearer '+ accessToken);
        newreq.setHeader('Content-Type','application/json');
        newreq.setBody(jsonstr);
        newreq.setEndpoint('callout:CallRestService' + '?' + jsonstr);
        newreq.setMethod('DELETE');
        
        //GetResponse from Lead Service
        HttpResponse res2 = httpObj.send(newreq);
        while (res2.getStatusCode() == 302) 
        {
        newreq.setEndpoint(res2.getHeader('Location'));
        res2 = new Http().send(newreq);
        }
        deserializeWrapper wrappObj = (deserializeWrapper)system.JSON.deserialize(res2.getBody(), deserializeWrapper.class);
        System.debug('deleteLead.isSuccess >> ' + wrappObj.isSuccess);
        System.debug('deleteLead.id >> ' + wrappObj.lead);
        System.debug('deleteLead.status >> ' + wrappObj.status);
 		return res2;
    }
    //This Method is used to Delete Lead through Rest Service 
    public static HttpResponse updateLead(String FirstName, String LastName, String Email, String Phone)
    {
        String jsonstr='{"FirstName":"'+ FirstName +'","LastName":"'+ LastName 
            +'","Email":"'+ Email +'","Phone":"'+ Phone +'"}';
        //Request for the Lead Service
        Http httpObj = new Http();
        HttpRequest newreq = new HttpRequest();
        String accessToken = CallLeadRestService.serviceAuthenticateToken();
        newreq.setHeader('Authorization','Bearer '+ accessToken);
        newreq.setHeader('Content-Type','application/json');
        newreq.setBody(jsonstr);
        newreq.setEndpoint('callout:CallRestService'+ '?' + '_HttpMethod=PATCH');
        newreq.setMethod('POST');
        
        //GetResponse from Lead Service
        HttpResponse res2 = httpObj.send(newreq);
        while (res2.getStatusCode() == 302) 
        {
        newreq.setEndpoint(res2.getHeader('Location'));
        res2 = new Http().send(newreq);
        }
        deserializeWrapper wrappObj = (deserializeWrapper)system.JSON.deserialize(res2.getBody(), deserializeWrapper.class);
        System.debug('deleteLead.isSuccess >> ' + wrappObj.isSuccess);
        System.debug('deleteLead.id >> ' + wrappObj.lead);
        System.debug('deleteLead.status >> ' + wrappObj.status);
        return res2;
    }
    
}