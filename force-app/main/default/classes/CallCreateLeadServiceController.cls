public with sharing class CallCreateLeadServiceController {
    public Lead leadObj{set;get;}
    public Boolean isSuccess{set;get;}
    public String leadId{set;get;}
    public String status{set;get;}
    public CallCreateLeadServiceController(){
        leadObj = new Lead();
    }
    public pageReference callCreateLead(){
        return Page.CallCreateLeadService;
    }
    public pageReference callUpdateLead(){
        return Page.CallUpdateLeadService;
    }
    public pageReference callDeleteLead(){
        return Page.CallDeleteLeadService;
    }
    public void createSave(){
        isSuccess = False;
        leadId = '';
        status = '';
        String firstName = leadObj.FirstName;
        String lastName = leadObj.LastName;
        String Email = leadObj.Email;
        String Phone = leadObj.Phone;
        soapSforceComSchemasClassLeadwebser.leadWrapper results =
            CallLeadSoapService.createLead(firstName, lastName, Email, Phone);
        isSuccess = results.isSuccess;
        leadId = results.lead;
        status = results.status;
    }
    public void updateSave(){
        isSuccess = False;
        leadId = '';
        status = '';
        String firstName = leadObj.FirstName;
        String lastName = leadObj.LastName;
        String Email = leadObj.Email;
        String Phone = leadObj.Phone;
        soapSforceComSchemasClassLeadwebser.leadWrapper results =
            CallLeadSoapService.updateLead(firstName, lastName, Email, Phone);
        isSuccess = results.isSuccess;
        leadId = results.lead;
        status = results.status;
    }
    public void deleteSave(){
        isSuccess = False;
        leadId = '';
        status = '';
        String firstName = leadObj.FirstName;
        String lastName = leadObj.LastName;
        String Email = leadObj.Email;
        String Phone = leadObj.Phone;
        soapSforceComSchemasClassLeadwebser.leadWrapper results =
            CallLeadSoapService.deleteLead(firstName, lastName, Email, Phone);
        isSuccess = results.isSuccess;
        leadId = results.lead;
        status = results.status;
    }
    
}