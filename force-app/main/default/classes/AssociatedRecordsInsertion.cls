/**
 * @File Name          : AssociatedRecordsInsertion.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 28/2/2020, 1:51:20 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/2/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
public with sharing class AssociatedRecordsInsertion {
    //This method Creates the Account and insert five contacts associated with Account
    public static void createAccountAssociateContact() {
        List<Account> accList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Salesforce';
        accList.add(acc);
        insert accList;
        List<Contact> conList = new List<Contact>();
        for(Integer i = 0 ; i < 5 ; i++) {
            Contact con = new Contact();
            con.LastName = 'ram'+ i;
            con.phone = '0123456789';
            con.Accountid = acc.id;
            conList.add(con);
        }
        insert conList;
    }
}