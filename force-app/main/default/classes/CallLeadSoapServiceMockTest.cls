/**
 * @File Name          : LeadWebServiceTest.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 4/4/2020, 12:26:49 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/3/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
@isTest
public class CallLeadSoapServiceMockTest {
    //Test the CreateLead Soap Service
    @isTest 
    static void testCreateLead() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new CallLeadSoapServiceMock());
        
        System.Test.startTest();
        soapSforceComSchemasClassLeadwebser.LeadWrapper results =  CallLeadSoapService.createLead('raghu',
                              'ram', 'raghuram@gmail.com', '9874563210');
        System.Test.stopTest();
        
        System.assertEquals(True, results.isSuccess);
        System.assertEquals('LeadIdReturnedSuccessfully', results.lead);
        System.assertEquals('Successs', results.status);       
    }
    //Test the UpdateLead Soap Service
    @isTest 
    static void testUpdateLead() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new CallLeadSoapServiceMock());
        
        System.Test.startTest();
        soapSforceComSchemasClassLeadwebser.LeadWrapper results =  CallLeadSoapService.updateLead('raghu',
                              'ram', 'raghuram@gmail.com', '9874563210');
        System.Test.stopTest();
        
        System.assertEquals(True, results.isSuccess);
        System.assertEquals('UpdateLeadIdReturnedSuccessfully', results.lead);
        System.assertEquals('Successs', results.status);       
    }
    //Test the DeleteLead Soap Service
    @isTest 
    static void testDeleteLead() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new CallLeadSoapServiceMock());
        
        System.Test.startTest();
        soapSforceComSchemasClassLeadwebser.LeadWrapper results =  CallLeadSoapService.deleteLead('raghu',
                              'ram', 'raghuram@gmail.com', '9874563210');
        System.Test.stopTest();
        
        System.assertEquals(True, results.isSuccess);
        System.assertEquals('Raghu Ram', results.lead);
        System.assertEquals('Successs', results.status);       
    }
}