/**
 * @File Name          : MathCalculator.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 15/2/2020, 3:02:19 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    15/2/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
public class MathCalculator {
    public void doMath(Integer a, Integer b){
        MathCalculator.printOutput(a,b);
    }
    private static Integer add(Integer a, Integer b){
        return a+b;
    }
    private static Integer multiply(Integer a, Integer b){
        return a*b;
    }
    private static Integer subtract(Integer a,Integer b){
        return a-b;
    }
    private static void printOutput(Integer a, Integer b){
        System.debug('After Addition >> '+ MathCalculator.add(a, b));
        System.debug('After Multiplication >> '+ MathCalculator.multiply(a, b));
        System.debug('After Subtraction >> '+ MathCalculator.subtract(a, b));
    }

}