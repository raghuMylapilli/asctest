/**
 * @File Name          : InvoiceLineItemHelper.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 28/2/2020, 3:33:41 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/2/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
public with sharing class InvoiceLineItemHelper {
    Double amount = 0;
    public void populateChildPrice(List<Invoice_Line_Item__c> invoiceItemList) {
        List<Id> invoiceIds = new List<Id>();
        for(Invoice_Line_Item__c item:invoiceItemList){
            if(item.Price__c !=null && item.Invoice__c != null){
                invoiceIds.add(item.Invoice__c);
            }
        }
        List<Invoice__c> invoiceList = [select id,Total_Amount__c from Invoice__c where id in :invoiceIds];
        List<Invoice_Line_Item__c> invoiceLineList = [select id,Price__c,Invoice__c from Invoice_Line_Item__c where Invoice__c in :invoiceIds ];
        for(Invoice__c pval : invoiceList){
            for(Invoice_Line_Item__c cval : invoiceLineList){
                if(cval.Invoice__c.equals(pval.id)){
                    amount += cval.Price__c;
                }  		 
            }
            pval.Total_Amount__c = amount;
            update pval;
        }
        
    }
}