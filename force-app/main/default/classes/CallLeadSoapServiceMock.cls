/**
 * @File Name          : LeadWebServiceTest.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 3/4/2020, 12:36:49 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/4/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
@isTest
global class CallLeadSoapServiceMock implements WebServiceMock {
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
           //Mock Login Response
           if(requestName == 'login'){
               partnerSoapSforceCom.login_element login_Ele = (partnerSoapSforceCom.login_element)request;
                partnerSoapSforceCom.loginResponse_element response_x = 
                    new partnerSoapSforceCom.loginResponse_element();
                partnerSoapSforceCom.LoginResult loginRes = new partnerSoapSforceCom.LoginResult();
               if(login_Ele.username == 'raghu.mylapilli@resourceful-wolf-j1flpr.com'){
                   
                   loginRes.sessionId = '00D280000016Hwz!ARgAQI4uwSSkAv6SdC.EBNvPOLutTnd3i9ye.p5fvjCv2SZvWpunItcjgx7lhfr.kaPL.FnRx9KbZRsajxCh0he5YgP.hL0f';
                   response_x.result = loginRes;
                   response.put('response_x',response_x);
               }
               else{
                   loginRes.sessionId = '';
                   response_x.result = loginRes;
                   response.put('response_x',response_x);

               }
                
            }
            //Mock Create Lead Response
            else if(requestName == 'createLead'){
                
               	soapSforceComSchemasClassLeadwebser.createLeadResponse_element leadRes = 
                   new soapSforceComSchemasClassLeadwebser.createLeadResponse_element();
                soapSforceComSchemasClassLeadwebser.LeadWrapper respElement = 
                    new soapSforceComSchemasClassLeadwebser.LeadWrapper();
                respElement.isSuccess = True;
                respElement.lead = 'LeadIdReturnedSuccessfully';
                respElement.status = 'Successs';
				leadRes.result = respElement;
                response.put('response_x',leadRes); 
            }
            //Mock Update Lead Response
            else if(requestName == 'updateLead'){
               
               	soapSforceComSchemasClassLeadwebser.updateLeadResponse_element leadRes =
                   new soapSforceComSchemasClassLeadwebser.updateLeadResponse_element();
                soapSforceComSchemasClassLeadwebser.LeadWrapper respElement = 
                    new soapSforceComSchemasClassLeadwebser.LeadWrapper();
                respElement.isSuccess = True;
                respElement.lead = 'UpdateLeadIdReturnedSuccessfully';
                respElement.status = 'Successs';
				leadRes.result = respElement;
                response.put('response_x',leadRes); 
            }
            //Mock DeleteLead Response
            else if(requestName == 'deleteLead'){
                
               	soapSforceComSchemasClassLeadwebser.deleteLeadResponse_element leadRes = 
                   new soapSforceComSchemasClassLeadwebser.deleteLeadResponse_element();
                soapSforceComSchemasClassLeadwebser.LeadWrapper respElement = 
                    new soapSforceComSchemasClassLeadwebser.LeadWrapper();
                respElement.isSuccess = True;
                respElement.lead = 'Raghu Ram';
                respElement.status = 'Successs';
				leadRes.result = respElement;
                response.put('response_x',leadRes); 
            }
            
        }
}