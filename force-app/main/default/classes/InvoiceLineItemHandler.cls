/**
 * @File Name          : InvoiceLineItemHandler.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 28/2/2020, 3:35:00 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/2/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
public with sharing class InvoiceLineItemHandler {

    private boolean m_IsExecuting = false;
    private integer batchSize = 0;

    public InvoiceLineItemHandler(boolean isExecuting, integer size) {
        m_IsExecuting = isExecuting;
        batchSize = size;
    }
    
    InvoiceLineItemHelper invoiceHelper = new InvoiceLineItemHelper();

    public void isBeforeInsert(){
		
    }
    public  void isBeforeUpdate(){
    }
    public void isBeforeDelete(){
    }
    public void isAfterInsert(){
        invoiceHelper.populateChildPrice(Trigger.New);
    }
    public void isAfterUpdate(){
    	invoiceHelper.populateChildPrice(Trigger.New);
    
    }
    public void isAfterDelete(){
        invoiceHelper.populateChildPrice(Trigger.old);
    
    }
    public void isAfterUnDelete(){
         invoiceHelper.populateChildPrice(Trigger.new);
    
    }

}