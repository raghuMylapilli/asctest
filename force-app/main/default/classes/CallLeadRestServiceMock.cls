/**
 * @File Name          : CallLeadRestServiceMock.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 31/3/2020, 12:25:27 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/3/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
@isTest
global class CallLeadRestServiceMock implements HttpCalloutMock {
    public Integer setCode ;
    public String setJson;
    //Constructor to get the StatusCode and JsonData as MockResponse
    global CallLeadRestServiceMock(Integer statusCode,String jsonData){
        setCode = statusCode;
        setJson = jsonData;
    }
    // Implement HttpCalloutMock interface method
    global HTTPResponse respond(HTTPRequest req) {
       
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(setJson);
        res.setStatusCode(setCode);
        return res;
    }
}