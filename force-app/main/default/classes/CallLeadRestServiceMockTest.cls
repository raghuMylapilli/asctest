/**
 * @File Name          : CallLeadRestServiceMockTest.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 1/4/2020, 12:25:27 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/3/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
@IsTest
public class CallLeadRestServiceMockTest {
    //Test the CreateLead MockFuntionality
    @isTest
    static void testCreateLead() {
        CallLeadRestServiceMock mockResponse = new CallLeadRestServiceMock(200,
                  '{"FirstName":"ram","LastName":"raghu","Email":"ram@gmail.com","Phone":"9874563210"}');
        //Let the test class context know where to get the response from
        Test.setMock(HttpCalloutMock.class, mockResponse);
        
        System.Test.startTest();
        HttpResponse responseMessage = CallLeadRestService.createLead('ram','raghu', 'ram@gmail.com', '9874563210'); 
        System.Test.stopTest();
        
        String strExpected = '{"FirstName":"ram","LastName":"raghu","Email":"ram@gmail.com","Phone":"9874563210"}';
        String contentType = responseMessage.getHeader('Content-Type');
        
        System.assert(contentType == 'application/json');
        System.assertEquals(200, responseMessage.getStatusCode());
        System.assertEquals(strExpected, responseMessage.getBody());
    }
    //Test the UpdateLead MockFuntionality
    @isTest
    static void testUpdateLead() {
        CallLeadRestServiceMock mockResponse = new CallLeadRestServiceMock(200,
                  '{"FirstName":"ram","LastName":"raghu","Email":"raghu@gmail.com","Phone":"0123654788"}');
        //Let the test class context know where to get the response from
        Test.setMock(HttpCalloutMock.class, mockResponse);
        
        System.Test.startTest();
        HttpResponse responseMessage = CallLeadRestService.updateLead('Raghu','ram', 'raghu@gmail.com', '0123654788'); 
        System.Test.stopTest();
        
        String strExpected = '{"FirstName":"ram","LastName":"raghu","Email":"raghu@gmail.com","Phone":"0123654788"}';
        String contentType = responseMessage.getHeader('Content-Type');
        
        System.assert(contentType == 'application/json');
        System.assertEquals(200, responseMessage.getStatusCode());
        System.assertEquals(strExpected, responseMessage.getBody());
    }
    //Test the DeleteLead MockFuntionality
    @isTest
    static void testDeleteLead() {
        CallLeadRestServiceMock mockResponse = new CallLeadRestServiceMock(200,
                  '{"FirstName":"ram","LastName":"raghu","Email":"raghu@gmail.com","Phone":"0123654788"}');
        //Let the test class context know where to get the response from
        Test.setMock(HttpCalloutMock.class, mockResponse);
        
        System.Test.startTest();
        HttpResponse responseMessage = CallLeadRestService.deleteLead('Raghu','ram', 'raghu@gmail.com', '0123654788'); 
        System.Test.stopTest();
        
        String strExpected = '{"FirstName":"ram","LastName":"raghu","Email":"raghu@gmail.com","Phone":"0123654788"}';
        String contentType = responseMessage.getHeader('Content-Type');
        
        System.assert(contentType == 'application/json');
        System.assertEquals(200, responseMessage.getStatusCode());
        System.assertEquals(strExpected, responseMessage.getBody());
    }
    //Test the FailureApex MockFuntionality
    @isTest
    static void testNegativeCase() {
        CallLeadRestServiceMock mockResponse = new CallLeadRestServiceMock(500,
                  '{"FirstName":"ram","LastName":"raghu","Email":"raghu@gmail.com","Phone":"0123654788"}');
        //Let the test class context know where to get the response from
        Test.setMock(HttpCalloutMock.class, mockResponse);
        
        System.Test.startTest();
        HttpResponse responseMessage = CallLeadRestService.deleteLead('Raghu','ram', 'raghu@gmail.com', '0123654788'); 
        System.Test.stopTest();
        
        String strExpected = '{"FirstName":"ram","LastName":"raghu","Email":"raghu@gmail.com","Phone":"0123654788"}';
        String contentType = responseMessage.getHeader('Content-Type');
        
        System.assert(contentType == 'application/json');
        System.assertEquals(500, responseMessage.getStatusCode());
        System.assertEquals(strExpected, responseMessage.getBody());
    }
}