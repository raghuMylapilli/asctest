/**
 * @File Name          : LeadWebServiceTest.cls
 * @Description        : 
 * @Author             : Raghu.Mylapilli@espl.com
 * @Group              : 
 * @Last Modified By   : Raghu.Mylapilli@espl.com
 * @Last Modified On   : 31/3/2020, 12:26:49 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/3/2020   Raghu.Mylapilli@espl.com     Initial Version
**/
public class CallLeadSoapService {
    //Get the SessionHeader by Authentication
    public static soapSforceComSchemasClassLeadwebser.SessionHeader_element ServiceAuthenticate(){
        String musername = 'raghu.mylapilli@resourceful-wolf-j1flpr.com';
        String password = '5738143Pr#9bnuUCnLeuwR5a9a1CZO8vmxe';
        partnerSoapSforceCom.Soap sp = new partnerSoapSforceCom.Soap();
        partnerSoapSforceCom.LoginResult loginResult = sp.login(musername,password);
        soapSforceComSchemasClassLeadwebser.SessionHeader_element sessionHeader = new soapSforceComSchemasClassLeadwebser.SessionHeader_element();
        sessionHeader.sessionId = loginResult.sessionId;
        return sessionHeader;
    
    }
    //This Method is used to Create Lead through Soap Service
    public static soapSforceComSchemasClassLeadwebser.LeadWrapper createLead(String FirstName,String LastName,String Email,String Phone)
    { 
        soapSforceComSchemasClassLeadwebser.LeadWebService  apexcall = new soapSforceComSchemasClassLeadwebser.LeadWebService();
        apexcall.SessionHeader = CallLeadSoapService.ServiceAuthenticate();
        soapSforceComSchemasClassLeadwebser.LeadWrapper results = apexcall.createLead(FirstName,LastName,Email,Phone);
        
        System.debug('createLead.isSuccess >> ' + results.isSuccess);
        System.debug('createLead.id >> ' + results.lead);
        System.debug('createLead.status >> ' + results.status);
        return results;
        
    }
    //This Method is used to delete Lead through Soap Service
    public static soapSforceComSchemasClassLeadwebser.LeadWrapper deleteLead(String FirstName,String LastName,String Email,String Phone)
    { 
        soapSforceComSchemasClassLeadwebser.LeadWebService  apexcall = new soapSforceComSchemasClassLeadwebser.LeadWebService();
        apexcall.SessionHeader = CallLeadSoapService.ServiceAuthenticate();
        soapSforceComSchemasClassLeadwebser.LeadWrapper results = apexcall.deleteLead(FirstName,LastName,Email,Phone);
        
        System.debug('deleteLead.isSuccess >> ' + results.isSuccess);
        System.debug('deleteLead.id >> ' + results.lead);
        System.debug('deleteLead.status >> ' + results.status); 
        return results;
    }
    //This Method is used to Update Lead through Soap Service
    public static soapSforceComSchemasClassLeadwebser.LeadWrapper updateLead(String FirstName,String LastName,String Email,String Phone)
    { 
        soapSforceComSchemasClassLeadwebser.LeadWebService  apexcall = new soapSforceComSchemasClassLeadwebser.LeadWebService();
        apexcall.SessionHeader = CallLeadSoapService.ServiceAuthenticate();
        soapSforceComSchemasClassLeadwebser.leadWrapper results = apexcall.updateLead(FirstName,LastName,Email,Phone);
        
        System.debug('updateLead.isSuccess >> ' + results.isSuccess);
        System.debug('updateLead.id >> ' + results.lead);
        System.debug('updateLead.status >> ' + results.status);
        return results;
    }   
}