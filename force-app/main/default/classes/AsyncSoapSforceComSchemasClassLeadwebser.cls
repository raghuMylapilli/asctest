//Generated by wsdl2apex

public class AsyncSoapSforceComSchemasClassLeadwebser {
    public class createLeadResponse_elementFuture extends System.WebServiceCalloutFuture {
        public soapSforceComSchemasClassLeadwebser.leadWrapper getValue() {
            soapSforceComSchemasClassLeadwebser.createLeadResponse_element response = (soapSforceComSchemasClassLeadwebser.createLeadResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.result;
        }
    }
    public class deleteLeadResponse_elementFuture extends System.WebServiceCalloutFuture {
        public soapSforceComSchemasClassLeadwebser.leadWrapper getValue() {
            soapSforceComSchemasClassLeadwebser.deleteLeadResponse_element response = (soapSforceComSchemasClassLeadwebser.deleteLeadResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.result;
        }
    }
    public class updateLeadResponse_elementFuture extends System.WebServiceCalloutFuture {
        public soapSforceComSchemasClassLeadwebser.leadWrapper getValue() {
            soapSforceComSchemasClassLeadwebser.updateLeadResponse_element response = (soapSforceComSchemasClassLeadwebser.updateLeadResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.result;
        }
    }
    public class AsyncLeadWebService {
        public String endpoint_x = 'https://ap15.salesforce.com/services/Soap/class/LeadWebService';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        public soapSforceComSchemasClassLeadwebser.DebuggingHeader_element DebuggingHeader;
        public soapSforceComSchemasClassLeadwebser.AllowFieldTruncationHeader_element AllowFieldTruncationHeader;
        public soapSforceComSchemasClassLeadwebser.CallOptions_element CallOptions;
        public soapSforceComSchemasClassLeadwebser.SessionHeader_element SessionHeader;
        public soapSforceComSchemasClassLeadwebser.DebuggingInfo_element DebuggingInfo;
        private String DebuggingHeader_hns = 'DebuggingHeader=http://soap.sforce.com/schemas/class/LeadWebService';
        private String AllowFieldTruncationHeader_hns = 'AllowFieldTruncationHeader=http://soap.sforce.com/schemas/class/LeadWebService';
        private String CallOptions_hns = 'CallOptions=http://soap.sforce.com/schemas/class/LeadWebService';
        private String SessionHeader_hns = 'SessionHeader=http://soap.sforce.com/schemas/class/LeadWebService';
        private String DebuggingInfo_hns = 'DebuggingInfo=http://soap.sforce.com/schemas/class/LeadWebService';
        private String[] ns_map_type_info = new String[]{'http://soap.sforce.com/schemas/class/LeadWebService', 'soapSforceComSchemasClassLeadwebser'};
        public AsyncSoapSforceComSchemasClassLeadwebser.createLeadResponse_elementFuture beginCreateLead(System.Continuation continuation,String FirstName,String LastName,String Email,String Phone) {
            soapSforceComSchemasClassLeadwebser.createLead_element request_x = new soapSforceComSchemasClassLeadwebser.createLead_element();
            request_x.FirstName = FirstName;
            request_x.LastName = LastName;
            request_x.Email = Email;
            request_x.Phone = Phone;
            return (AsyncSoapSforceComSchemasClassLeadwebser.createLeadResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSoapSforceComSchemasClassLeadwebser.createLeadResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/schemas/class/LeadWebService',
              'createLead',
              'http://soap.sforce.com/schemas/class/LeadWebService',
              'createLeadResponse',
              'soapSforceComSchemasClassLeadwebser.createLeadResponse_element'}
            );
        }
        public AsyncSoapSforceComSchemasClassLeadwebser.deleteLeadResponse_elementFuture beginDeleteLead(System.Continuation continuation,String FirstName,String LastName,String Email,String Phone) {
            soapSforceComSchemasClassLeadwebser.deleteLead_element request_x = new soapSforceComSchemasClassLeadwebser.deleteLead_element();
            request_x.FirstName = FirstName;
            request_x.LastName = LastName;
            request_x.Email = Email;
            request_x.Phone = Phone;
            return (AsyncSoapSforceComSchemasClassLeadwebser.deleteLeadResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSoapSforceComSchemasClassLeadwebser.deleteLeadResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/schemas/class/LeadWebService',
              'deleteLead',
              'http://soap.sforce.com/schemas/class/LeadWebService',
              'deleteLeadResponse',
              'soapSforceComSchemasClassLeadwebser.deleteLeadResponse_element'}
            );
        }
        public AsyncSoapSforceComSchemasClassLeadwebser.updateLeadResponse_elementFuture beginUpdateLead(System.Continuation continuation,String FirstName,String LastName,String Email,String Phone) {
            soapSforceComSchemasClassLeadwebser.updateLead_element request_x = new soapSforceComSchemasClassLeadwebser.updateLead_element();
            request_x.FirstName = FirstName;
            request_x.LastName = LastName;
            request_x.Email = Email;
            request_x.Phone = Phone;
            return (AsyncSoapSforceComSchemasClassLeadwebser.updateLeadResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSoapSforceComSchemasClassLeadwebser.updateLeadResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/schemas/class/LeadWebService',
              'updateLead',
              'http://soap.sforce.com/schemas/class/LeadWebService',
              'updateLeadResponse',
              'soapSforceComSchemasClassLeadwebser.updateLeadResponse_element'}
            );
        }
    }
}